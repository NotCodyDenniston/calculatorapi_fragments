package com.example.navcomponentfragment.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.navigation.findNavController
import com.example.navcomponentfragment.R
import com.example.navcomponentfragment.ui.theme.NavComponentFragmentTheme


/**
 * A simple [Fragment] subclass.
 *
 */
class DashboardFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                NavComponentFragmentTheme {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Button(onClick = { findNavController().navigate(R.id.sumFragment) }) {
                            Text(text = "Navigate to the sum fragment")
                        }
                        Button(onClick = { findNavController().navigate(R.id.differenceFragment) }) {
                            Text(text = "Navigate to the difference fragment")
                        }
                        Button(onClick = { findNavController().navigate(R.id.productFragment) }) {
                            Text(text = "Navigate to the product fragment")
                        }
                        Button(onClick = { findNavController().navigate(R.id.quotientFragment) }) {
                            Text(text = "Navigate to the quotient fragment")
                        }
                    }

                    // To go forward Activity -> Intent(this, ActivityToGoToo::class.java)
                    // Fragment to Fragment (with Navigation Component)
                    // -> findNavController().navigate(R.id.destinationId)

                    // To go back Activity -> finish()
                    // Fragment (with Navigation component) findNavController().navigateUp()

                }
            }
        }
    }
}