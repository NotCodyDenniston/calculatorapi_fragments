package com.example.navcomponentfragment.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.navigateUp
import com.example.navcomponentfragment.R
import com.example.navcomponentfragment.ui.theme.NavComponentFragmentTheme
import com.example.navcomponentfragment.view.MainActivity
import com.example.navcomponentfragment.viewmodel.MainViewModel
import com.google.android.material.internal.ContextUtils.getActivity

class SumFragment : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            val mainViewModel: MainViewModel by viewModels()
            setContent {
                val result = mainViewModel.equationState.collectAsState().value
                    NavComponentFragmentTheme {
                        Box(contentAlignment = Alignment.Center) {
                            Button(onClick = { findNavController().navigateUp()}) {
                                Text(text = "Go Back")
                            }
                            SumScreen(mainViewModel = mainViewModel,
                                equationState = result,
                                navigate = { findNavController().navigateUp() })
                        }

                    // To go forward Activity -> Intent(this, ActivityToGoToo::class.java)
                    // Fragment to Fragment (with Navigation Component)
                    // -> findNavController().navigate(R.id.destinationId)

                    // To go back Activity -> finish()
                    // Fragment (with Navigation component) findNavController().navigateUp()

                }
            }
        }
    }
}

@SuppressLint("RestrictedApi")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SumScreen(mainViewModel: MainViewModel,
              equationState: Double,
              navigate: () -> Unit){
    val activity = LocalContext.current as? Activity
    Column(verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        val textState = remember { mutableStateOf("") }
        val secondTextState = remember { mutableStateOf("") }
        Row(horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()) {
            TextField(
                value = textState.value,
                onValueChange = { textState.value = it },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier.size(100.dp)
            )
            TextField(
                value = secondTextState.value,
                onValueChange = { secondTextState.value = it },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier.size(100.dp)
            )
        }

        Button(
            colors = ButtonDefaults.buttonColors(containerColor = Color.Red),
            onClick = {
                mainViewModel.EvaluateExpression("${textState.value}+${secondTextState.value}")
                Toast.makeText(activity,"${textState.value.toDouble() + secondTextState.value.toDouble()}",Toast.LENGTH_LONG).show()
                navigate()
            }
        ) {
            Text(text = "Evaluate Sum")
        }
        Text(text = "${equationState}")
    }
}
